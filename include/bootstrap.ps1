

Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
Install-Module -Name AzureRM -AllowClobber -Force



# Moving CDROM drive
$cdrom_drive_letter = "${cdrom_drive_letter}"

$drv = Get-WmiObject win32_volume -Filter 'DriveType=5'
$drv.DriveLetter = "$($cdrom_drive_letter):"
$drv.Put()


# Mount drives
$letters = "${drive_letters}"
$labels = "${drive_labels}"

New-Item c:\drives.txt -type file  -force -value $letters
New-Item c:\names.txt -type file  -force -value $labels


$letters = (Get-Content -Path C:\drives.txt) -split ','

$labels = (Get-Content -Path C:\names.txt) -split ','

$disks = Get-Disk | Where-Object partitionstyle -eq 'raw' | Sort-Object Location

$count = 0

foreach ($disk in $disks) {
	$driveLetter = $letters[$count]
	$disk | 
	Initialize-Disk -PartitionStyle MBR -PassThru |
	New-Partition -UseMaximumSize -DriveLetter $driveLetter |
	Format-Volume -FileSystem NTFS -NewFileSystemLabel $labels[$count] -Confirm:$false -Force
	$count++
}
