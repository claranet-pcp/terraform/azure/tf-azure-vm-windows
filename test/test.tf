terraform {
  required_version = "~> 0.14.3"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.41.0"
    }
  }
}

provider "azurerm" {
  features {}
}

resource azurerm_resource_group "test01_vm" {
  name     = "test01vm"
  location = var.location
  tags     = var.tags
}

module test01_vm {
  source              = "../"
  resource_group_name = azurerm_resource_group.test01_vm.name
  location            = var.location
  resource_identifier = "test"
  computer_name       = "test"
  vm_disk_type        = "Premium_LRS"
  admin_password      = "clara_vmtest01"
  tags                = var.tags
  az_support          = false
  vm_count            = 2
  vm_size             = "Standard_D2s_v3"
  image_sku           = "2019-Datacenter"
  oms_extension       = false
  subnet_id           = "/subscriptions/<subid>/resourceGroups/testvnet/providers/Microsoft.Network/virtualNetworks/testvnet/subnets/backend"
  drive_letters       = var.drive_letters
  drive_sizes         = var.drive_sizes
  drive_labels        = var.drive_labels
}

variable location {
  default = "uksouth"
}

variable drive_letters {
  default = ["g", "h"]
}

variable drive_sizes {
  default = ["10", "20"]
}

variable drive_labels {
  default = ["g", "h"]
}

variable tags {
  default = {
    Environment = "vm_module_test"
    Source      = "DevOps"
  }
}

variable module_version {
  default = ""
}
