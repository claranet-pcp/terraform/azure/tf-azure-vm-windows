resource "azurerm_virtual_machine_extension" "oms" {
  count                      = var.vm_count * (var.oms_extension == true ? 1 : 0)
  depends_on                 = [azurerm_windows_virtual_machine.windows_vm]
  name                       = "OMSExtension"
  auto_upgrade_minor_version = true
  type                       = "MicrosoftMonitoringAgent"
  type_handler_version       = "1.0"
  tags                       = var.tags
  virtual_machine_id         = element(azurerm_windows_virtual_machine.windows_vm.*.id, count.index)
  publisher                  = "Microsoft.EnterpriseCloud.Monitoring"

  settings = <<SETTINGS
     {
		    "workspaceId": "${var.log_analytics_workspace_id}"
		}
SETTINGS

  protected_settings = <<SETTINGS
      {
        "workspaceKey": "${var.log_analytics_workspace_key}"
      }
    SETTINGS
}
