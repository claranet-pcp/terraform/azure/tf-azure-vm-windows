resource "azurerm_virtual_machine_extension" "vm_domain_join" {
  count                = var.vm_count * (var.domain_join == true ? 1 : 0)
  depends_on           = [azurerm_windows_virtual_machine.windows_vm]
  name                 = "${format("${var.resource_identifier}%03d", count.index + 1)}-domain-join"
  publisher            = "Microsoft.Compute"
  type                 = "JsonADDomainExtension"
  type_handler_version = "1.3"
  tags                 = var.tags
  virtual_machine_id   = element(azurerm_windows_virtual_machine.windows_vm.*.id, count.index)

  settings = <<SETTINGS
  {
    "Name": "${var.ad_domain_name}",
    "User": "${var.ad_domain_join_user}@${var.ad_domain_name}",
    "OUPath": "${var.ad_ou_path}",
    "Restart": "true",
    "Options" :  "3"
}
SETTINGS

  protected_settings = <<SETTINGS
      {
        "Password": "${var.domain_join_pw}"
      }
    SETTINGS
}
