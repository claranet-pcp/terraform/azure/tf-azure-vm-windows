resource "azurerm_virtual_machine_extension" "sql_iaas" {
  count                      = var.vm_count * (var.sql_iaas_extension_enable == true ? 1 : 0)
  name                       = "SqlIaasExtension"
  depends_on                 = [azurerm_windows_virtual_machine.windows_vm]
  publisher                  = "Microsoft.SqlServer.Management"
  type                       = "SqlIaaSAgent"
  type_handler_version       = "2.0"
  auto_upgrade_minor_version = true
  virtual_machine_id         = element(azurerm_windows_virtual_machine.windows_vm.*.id, count.index)

  settings = <<SETTINGS
    {
    "AutoPatchingSettings": {
      "PatchCategory": "WindowsMandatoryUpdates",
      "Enable": "${var.sql_enable_auto_patch}",
      "DayOfWeek": "${local.sql_patch_day}",
      "MaintenanceWindowStartingHour": "${local.sql_patch_start_hour}",
      "MaintenanceWindowDuration": "${local.sql_patch_duration}"
    },
    "AutoBackupSettings": {
      "Enable": ${var.sql_enable_auto_backup},
      "RetentionPeriod": "${local.sql_backup_retention}",
      "EnableEncryption": "${local.sql_backup_encryption}",
      "BackupSystemDbs" : "${local.sql_backup_systemdbs}",
      "BackupScheduleType" : "${local.sql_backup_type}"
      }
    }
SETTINGS

  protected_settings = <<SETTINGS
{
  "StorageUrl": "${var.sql_backup_storage_account}",
  "StorageAccessKey": "${var.sql_backup_storage_access_key}",
  "Password": "${var.sql_backup_encryption_password}"
}
  SETTINGS
}
