
locals {

  short_region                  = var.location_lookup[(replace(var.location, " ", ""))]
  boot_strap                    = "${data.template_file.bootstrap.rendered}${var.boot_script}"
  resource_identifier           = var.resource_identifier
  resource_identifier_no_hyphen = replace(local.resource_identifier, "-", "")

  sql_patch_day         = var.sql_enable_auto_patch == true ? var.sql_patch_day : ""
  sql_patch_start_hour  = var.sql_enable_auto_patch == true ? var.sql_patch_start_hour : ""
  sql_patch_duration    = var.sql_enable_auto_patch == true ? var.sql_patch_duration : ""
  sql_backup_retention  = var.sql_enable_auto_backup == true ? var.sql_backup_retention : ""
  sql_backup_encryption = var.sql_enable_auto_backup == true ? var.sql_backup_encryption : ""
  sql_backup_systemdbs  = var.sql_enable_auto_backup == true ? var.sql_backup_systemdbs : ""
  sql_backup_type       = var.sql_enable_auto_backup == true ? "automated" : ""


  disk_list_of_maps = flatten([
    for vms in azurerm_windows_virtual_machine.windows_vm.*.id : [
      for disks in var.drive_letters : {
        vms   = vms
        disks = disks
        size  = element(var.drive_sizes, index(var.drive_letters, disks))
        lun   = 10 + index(var.drive_letters, disks)
        zone  = var.az_support == true ? element(["1", "2", "3"], index(azurerm_windows_virtual_machine.windows_vm.*.id, vms)) : null
      }
    ]
  ])
}

