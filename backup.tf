resource "azurerm_backup_protected_vm" "windows_backups" {
  count               = var.vm_count * (var.backup_enabled == true ? 1 : 0)
  depends_on          = [azurerm_network_interface.windows_vm]
  resource_group_name = var.recovery_vault_rg_name
  recovery_vault_name = var.recovery_vault_name
  source_vm_id        = azurerm_windows_virtual_machine.windows_vm[count.index].id
  backup_policy_id    = var.backup_policy_id
}