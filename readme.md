# **Azure Windows Virtual Machine Module**

## **What is it?**

This module is to simplify the provisioning of one or more Windows based virtual machines.

This module features the following:

```
* Support for deploying mulitple virtual machines
* Support for multiple disks
* Availability zone support
* Availability set support
* Virtual Machine backup
* Support for Accelerated Networking
* Support for Marketplace images
* Support for Domain Join
* Various VM Extensions
```

# **Requirements**

Terraform 0.12 is the minimum requirements version for this module. This code has no external/other dependencies

# **Future Improvements**

- Add a lifecycle block for the relevant resources - so that we can avoid issues if someone replaces a nic or a drive etc
- turn the drive info in to a list of maps - so that we can easily see the drive geometry rather than having multiple lists

# **Inputs**

This module has varied inputs which are split into the following sections:-

- virtual machine(s)
- disks
- backups
- automation
- placement
- operating system
- network interface(s)
- extenstions

# **virtual machine(s)**

## **generic settings**

| Name                      | Description                                                              |  Type  |      Default      | Required |
| ------------------------- | ------------------------------------------------------------------------ | :----: | :---------------: | :------: |
| resource_group_name       | Specifies the name of the Resource Group in which the VM(s) should exist | string |        n/a        |  _yes_   |
| vm_count                  | The number of VMs you wish to create                                     | number |         1         |    no    |
| location                  | Specifies the Azure Region where the Virtual Machine exists              | string |   "northeurope"   |    no    |
| vm_size                   | Specifies the Azure Region where the VM(s) exists                        | string | "Standard_DS1_v2" |    no    |
| hybrid_benefit_enabled    | Species enable Azure hybrid benefit                                      |  bool  |       false       |    no    |
| license_type              | Specifies the BYOL Type for this Virtual Machine                         | string | "Windows_Server"  |    no    |
| enable_automatic_upgrades | Are automatic updates enabled on this VM(s)?                             | string |       false       |    no    |

## **tags**

| Name | Description                              | Type | Default | Required |
| ---- | ---------------------------------------- | :--: | :-----: | :------: |
| tags | A mapping of tags to assign to the VM(s) | map  |   {}    |    no    |
| tags | A mapping of tags to assign to the VM(s) | map  |   {}    |    no    |

## **naming**

| Name                       | Description                                            |  Type  | Default | Required |
| -------------------------- | ------------------------------------------------------ | :----: | :-----: | :------: |
| computer_name_suffix_start | number from which to start the VM computer name suffix | string |   ""    |    no    |
| computer_name              | computer name                                          | string |   n/a   |  _yes_   |
| resource_identifier        | identifier for Azure VM resource                       | string |   n/a   |  _yes_   |
| resource_name_suffix_start | number from which to start the resource id suffix      | string |   ""    |    no    |

# **disks**

## **generic**

| Name               | Description                                                 |  Type  | Default | Required |
| ------------------ | ----------------------------------------------------------- | :----: | :-----: | :------: |
| vm_disk_type       | Specifies the type of managed disk to create for all drives | string |   n/a   |  _yes_   |
| cdrom_drive_letter | Specifies a drive letter on which to mount the CD-ROM       | string |   "v"   |    no    |

## **os_disk**

| Name                          | Description                                                                                               |  Type  |   Default   | Required |
| ----------------------------- | --------------------------------------------------------------------------------------------------------- | :----: | :---------: | :------: |
| os_caching                    | Specifies the caching requirements for the OS Disk                                                        | string | "ReadWrite" |    no    |
| os_create_option              | Specifies how the OS Disk should be created                                                               | string | "FromImage" |    no    |
| delete_os_disk_on_termination | Should the OS Disk (either the Managed Disk / VHD Blob) be deleted when the Virtual Machine is destroyed? |  bool  |    true     |    no    |

## **storage_data_disk**

| Name                             | Description                                                                                                    | Type | Default | Required |
| -------------------------------- | -------------------------------------------------------------------------------------------------------------- | :--: | :-----: | :------: |
| drive_letters                    | Specifies a list of drive letters to mount on the VM(s)                                                        | list |   []    |    no    |
| drive_sizes                      | Specifies a list of drive sizes to mount on the VM(s)                                                          | list |   []    |    no    |
| drive_labels                     | Specifies a list of drive labels to mount on the VM(s)                                                         | list |   []    |    no    |
| delete_data_disks_on_termination | Should the Data Disks (either the Managed Disks / VHD Blobs) be deleted when the Virtual Machine is destroyed? | bool |  true   |    no    |

# **backups**

## **settings**

| Name                   | Description                                                                             |  Type  | Default | Required |
| ---------------------- | --------------------------------------------------------------------------------------- | :----: | :-----: | :------: |
| backup_enabled         | Enable VM backups                                                                       |  bool  |  false  |    no    |
| recovery_vault_rg_name | The name of the resource group in which to create the Recovery Services Protected VM(s) | string |   ""    |    no    |
| recovery_vault_name    | Specifies the name of the Recovery Services Vault to use                                | string |   ""    |    no    |
| backup_policy_id       | Specifies the id of the backup policy to use                                            | string |   ""    |    no    |

# **automation**

## **settings**

|bootstrap|Enable boostrapping of VM(s)|bool|false|no|
|boot_script|Specifies rendered script to be run on 1st boot|string|""|no|

# **placement**

## **availability zones**

| Name       | Description       | Type | Default | Required |
| ---------- | ----------------- | :--: | :-----: | :------: |
| az_support | Enable az support | bool |  true   |    no    |

## **availability sets**

| Name                         | Description                                                                      |  Type  | Default | Required |
| ---------------------------- | -------------------------------------------------------------------------------- | :----: | :-----: | :------: |
| availability_set             | Enables Availability Set                                                         |  bool  |  false  |    no    |
| availability_set_id          | Availability Set id. If not passed then new Availability set is created          | string |  null   |    no    |
| platform_fault_domain_count  | Specifies the number of fault domains that are used                              | number |    3    |    no    |
| platform_update_domain_count | Specifies the number of update domains that are use                              | number |    5    |    no    |
| diags_storage_account_uri    | The Storage Account's Blob Endpoint which should hold the VM(s) diagnostic files | string |   ""    |    no    |

# **operating system**

## **image**

| Name            | Description                                                                 |  Type  |         Default          | Required |
| --------------- | --------------------------------------------------------------------------- | :----: | :----------------------: | :------: |
| image_publisher | Specifies the publisher of the image used to create the VM(s)               | string | "MicrosoftWindowsServer" |    no    |
| image_offer     | Specifies the offer of the image used to create the VM(s)                   | string |     "WindowsServer"      |    no    |
| image_sku       | Specifies the SKU of the image used to create the VM(s)                     | string |    "2016-Datacenter"     |    no    |
| image_version   | Specifies the version of the image used to create the VM(s)                 | string |         "latest"         |    no    |
| custom_image_id | Specifies the ID of the Custom Image which the VM(s) should be created from | string |           null           |    no    |

## **admin**

| Name           | Description                                                  |  Type  |    Default    | Required |
| -------------- | ------------------------------------------------------------ | :----: | :-----------: | :------: |
| admin_username | Specifies the name of the local administrator account        | string | "azure.admin" |    no    |
| admin_password | The password associated with the local administrator account | string |      n/a      |  _yes_   |

# **network interface(s)**

## **connected resources**

| Name                                          | Description                                                                                 |  Type  | Default | Required |
| --------------------------------------------- | ------------------------------------------------------------------------------------------- | :----: | :-----: | :------: |
| subnet_id                                     | Reference to a subnet in which the NIC(s) have been created                                 | string |   n/a   |  _yes_   |
| nsg_id                                        | The ID of the Network Security Group to associate with the network interface(s)             | string |   ""    |    no    |
| load_balancer_backend_address_pools_ids       | List of Load Balancer Backend Address Pool IDs references to which this NIC(s) belong       |  list  |   []    |    no    |
| application_gateway_backend_address_pools_ids | List of Application Gateway Backend Address Pool IDs references to which this NIC(s) belong |  list  |   []    |    no    |
| enable_nsg                                    | Enable nsg bind to NIC(s) defined via nsg_id                                                |  bool  |  false  |    no    |
| enable_loadbalancer_bep                       | Enable lodbalancer bind to NIC(s) defined via load_balancer_backend_address_pools_ids       |  bool  |  false  |    no    |
| enable_appgateway_bep                         | Enable appgateway bind to NIC(s) define via gateway_backend_address_pools_ids               |  bool  |  false  |    no    |


## **configuration**

| Name                          | Description                                       |  Type  |  Default  | Required |
| ----------------------------- | ------------------------------------------------- | :----: | :-------: | :------: |
| public_ip_enabled             | Enable public ip address                          |  bool  |   false   |    no    |
| public_ip_address_allocation  | Defines how a public IP address(es) is assigned   | string | "Static"  |    no    |
| private_ip_address_allocation | Defines how a private IP address(es) is assigned  | string | "Dynamic" |    no    |
| enable_accelerated_networking | Enables Azure Accelerated Networking using SR-IOV |  bool  |   false   |    no    |

# **extensions/agents**

## **VM agent**

| Name               | Description                                                              |  Type  | Default | Required |
| ------------------ | ------------------------------------------------------------------------ | :----: | :-----: | :------: |
| provision_vm_agent | Should the Azure Virtual Machine Guest Agent be installed on this VM(s)? | string |  true   |    no    |

## **domain join**

| Name                | Description                                    |  Type  | Default | Required |
| ------------------- | ---------------------------------------------- | :----: | :-----: | :------: |
| domain_join         | Enable domain join                             |  bool  |  false  |    no    |
| ad_domain_name      | Specifies active directory domain name         | string |   ""    |    no    |
| ad_domain_join_user | Specifies user to join computers to domain     | string |   ""    |    no    |
| domain_join_pw      | Specifies domain join password                 | string |   ""    |    no    |
| ad_ou_path          | Specifies ou to deploy new computer account(s) | string |   ""    |    no    |

## **oms extension**

| Name                        | Description                                   |  Type  | Default | Required |
| --------------------------- | --------------------------------------------- | :----: | :-----: | :------: |
| oms_extension               | Enable OMS extension                          |  bool  |  true   |    no    |
| log_analytics_workspace_id  | Log Analytics Workspace id for OMS extension  | string |   n/a   |  _yes_   |
| log_analytics_workspace_key | Log Analytics Workspace key for OMS extension | string |   n/a   |  _yes_   |

## **iaas diags**

| Name                 | Description                               |  Type  | Default | Required |
| -------------------- | ----------------------------------------- | :----: | :-----: | :------: |
| iaas_diags_extension | Enable IaaS Diags extension               |  bool  |  false  |    no    |
| storage_account_name | IaaS Diags extension Storage Account name | string |   ""    |    no    |
| storage_account_key  | IaaS Diags extension Storage Account key  | string |   ""    |    no    |

## **sql iaas**

| Name                           | Description                                                                |  Type  | Default  | Required |
| ------------------------------ | -------------------------------------------------------------------------- | :----: | :------: | :------: |
| sql_iaas_extension_enable      | Enable the SQL IaaS agent for automated backups and patching of SQL Server |  bool  |  false   |    no    |
| sql_enable_auto_patch          | Enable or disable automatic SQL patching                                   |  bool  |   true   |    no    |
| sql_patch_day                  | Day of week to automatically patch SQL                                     | string | "Sunday" |    no    |
| sql_patch_start_hour           | Maintenance window start hour                                              | number |    0     |    no    |
| sql_patch_duration             | Maintenance window duration in minutes                                     | number |    60    |    no    |
| sql_enable_auto_backup         | Enable SQL auto backup                                                     |  bool  |   true   |    no    |
| sql_backup_retention           | Retention in days of automated SQL backups                                 | number |    30    |    no    |
| sql_backup_encryption          | Enable or disable encryption of sql backups                                |  bool  |   true   |    no    |
| sql_backup_systemdbs           | Enable or disable backups of system dbs                                    |  bool  |   true   |    no    |
| sql_backup_storage_account     | Storage account url                                                        | string |    ""    |    no    |
| sql_backup_encryption_password | Password used to encrypt SQL backups                                       | string |    ""    |    no    |
| sql_backup_storage_access_key  | Storage account access key                                                 | string |    ""    |    no    |

## **dependency agent**

| Name                     | Description                            | Type | Default | Required |
| ------------------------ | -------------------------------------- | :--: | :-----: | :------: |
| dependency_agent_enabled | Enable or disable the dependency agent | bool |  false  |    no    |

# **Example**

```
#This example produces a single VM based on the latest Windows 10 RS5 image.

module "win_vm" {
  source              = "git::https://gitlab.com/claranet-pcp/terraform/azure/tf-azure-vm-windows?ref=v2.1.0"
  resource_group_name = azurerm_resource_group.test_rsg.name
  location            = var.location
  vm_name_override    = "testsvr"
  computer_name       = "testsvr"
  vm_disk_type        = "StandardSSD_LRS"
  admin_username      = var.admin_username
  admin_password      = "REPLACE_ME"
  domain_join         = false
  image_publisher     = "MicrosoftWindowsServer"
  image_offer         = "WindowsServer"
  image_sku           = "2019-Datacenter"
  image_version       = "latest"
  subnet_id           = var.subnet_id
  az_support          = false
  vm_size             = "Standard_D2s_v3"
  drive_letters       = ["e"]
  drive_sizes         = ["20"]
  drive_labels        = ["data"]
  oms_extension       = false
  tags                = local.standard_tags
}

```
