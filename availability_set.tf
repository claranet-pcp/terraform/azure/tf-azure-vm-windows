resource "azurerm_availability_set" "vm" {
  count                        = (var.availability_set == true ? 1 : 0)
  name                         = "as-${var.resource_identifier}"
  platform_update_domain_count = var.platform_update_domain_count
  platform_fault_domain_count  = var.platform_fault_domain_count
  location                     = var.location
  resource_group_name          = var.resource_group_name
  managed                      = true
  tags                         = var.tags
}
