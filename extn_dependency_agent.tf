resource "azurerm_virtual_machine_extension" "dependency_agent" {
  count                      = var.vm_count * (var.dependency_agent_enabled == true ? 1 : 0)
  depends_on                 = [azurerm_windows_virtual_machine.windows_vm]
  name                       = "DependencyAgentWindows"
  publisher                  = "Microsoft.Azure.Monitoring.DependencyAgent"
  type                       = "DependencyAgentWindows"
  type_handler_version       = "9.5"
  auto_upgrade_minor_version = true
  tags                       = var.tags
  virtual_machine_id         = element(azurerm_windows_virtual_machine.windows_vm.*.id, count.index)
}
