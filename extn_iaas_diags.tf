data "template_file" "xml" {
  template = file("${path.module}/include/windows-config.xml.tmpl")
}

resource "azurerm_virtual_machine_extension" "iaas_diagnostics" {
  count                      = var.vm_count * (var.iaas_diags_extension == true ? 1 : 0)
  depends_on                 = [azurerm_windows_virtual_machine.windows_vm]
  name                       = "IaaSDiagnostics"
  publisher                  = "Microsoft.Azure.Diagnostics"
  type                       = "IaaSDiagnostics"
  type_handler_version       = "1.16"
  auto_upgrade_minor_version = true
  virtual_machine_id         = element(azurerm_windows_virtual_machine.windows_vm.*.id, count.index)

  settings = <<SETTINGS
    {
        "xmlCfg": "${base64encode(data.template_file.xml.rendered)}",
        "storageAccount": "${var.storage_account_name}"
    }
SETTINGS

  protected_settings = <<SETTINGS
    {
        "storageAccountName": "${var.storage_account_name}",
        "storageAccountKey": "${var.storage_account_key}"
    }
SETTINGS
}
