resource "azurerm_windows_virtual_machine" "windows_vm" {
  count                 = var.vm_name_override == "" ? var.vm_count : 1
  name                  = var.vm_name_override == "" ? ((var.resource_name_suffix_start == "" ? format("vm-${var.resource_identifier}-%03d", count.index + 1) : format("vm-${var.resource_identifier}-%03d", count.index + var.resource_name_suffix_start))) : "vm-${var.vm_name_override}"
  location              = var.location
  resource_group_name   = var.resource_group_name
  network_interface_ids = [element(azurerm_network_interface.windows_vm.*.id, count.index)]
  size                  = var.vm_size
  zone                  = var.az_support == true ? element(["1", "2", "3"], count.index) : null
  availability_set_id   = (var.availability_set == true ? azurerm_availability_set.vm[0].id : var.availability_set_id)
  license_type          = var.hybrid_benefit_enabled == true ? var.license_type : null
  tags                  = var.tags

  source_image_reference {
    publisher = var.custom_image_id == null ? var.image_publisher : null
    offer     = var.custom_image_id == null ? var.image_offer : null
    sku       = var.custom_image_id == null ? var.image_sku : null
    version   = var.custom_image_id == null ? var.image_version : null
  }

  source_image_id = var.custom_image_id

  os_disk {
    name                 = var.vm_name_override == "" ? ((var.resource_name_suffix_start == "" ? "${format("disk-${var.resource_identifier}-%03d", count.index + 1)}-os" : "${format("disk-${var.resource_identifier}-%03d", count.index + var.resource_name_suffix_start)}-os")) : "disk-${var.vm_name_override}-os"
    caching              = var.os_caching
    storage_account_type = var.vm_disk_type
  }

  computer_name            = var.computer_name_override == "" ? ((var.computer_name_suffix_start == "" ? format("${var.computer_name}%03d", count.index + 1) : format("${var.computer_name}%03d", count.index + var.computer_name_suffix_start))) : var.computer_name_override
  admin_username           = var.admin_username
  admin_password           = var.admin_password
  provision_vm_agent       = var.provision_vm_agent
  enable_automatic_updates = var.enable_automatic_upgrades
  timezone                 = var.timezone_lookup[var.location]

  dynamic "boot_diagnostics" {
    for_each = var.diags_storage_account_uri == "" ? [] : ["single_entry_list"]
    content {
      storage_account_uri = var.diags_storage_account_uri
    }
  }

  dynamic "identity" {
    for_each = var.system_identity_enabled ? ["enabled"] : []
    content {
      type = "SystemAssigned"
    }
  }
}


resource "azurerm_managed_disk" "datadisk" {
  count                = length(local.disk_list_of_maps)
  zones                = var.az_support == true ? [element(local.disk_list_of_maps, count.index)["zone"]] : null
  location             = var.location
  resource_group_name  = var.resource_group_name
  storage_account_type = "Standard_LRS"
  name                 = var.vm_name_override == "" ? ((var.resource_name_suffix_start == "" ? "${format("disk-${var.resource_identifier}-%03d", count.index + 1)}-data-${element(local.disk_list_of_maps, count.index)["disks"]}" : "${format("disk-${var.resource_identifier}-%03d", count.index + var.resource_name_suffix_start)}-data-${element(local.disk_list_of_maps, count.index)["disks"]}")) : "disk-${var.vm_name_override}-data-${element(local.disk_list_of_maps, count.index)["disks"]}"
  create_option        = "Empty"
  disk_size_gb         = element(local.disk_list_of_maps, count.index)["size"]
  depends_on           = [azurerm_windows_virtual_machine.windows_vm]
}

resource "azurerm_virtual_machine_data_disk_attachment" "datadisk_attachment" {
  count              = length(local.disk_list_of_maps)
  managed_disk_id    = element(azurerm_managed_disk.datadisk.*.id, count.index)
  virtual_machine_id = element(local.disk_list_of_maps, count.index)["vms"]
  lun                = element(local.disk_list_of_maps, count.index)["lun"]
  caching            = "ReadWrite"
}

resource "azurerm_network_interface" "windows_vm" {
  count                         = var.vm_name_override == "" ? var.vm_count : 1
  name                          = var.vm_name_override == "" ? ((var.resource_name_suffix_start == "" ? format("nic-${var.resource_identifier}-%03d", count.index + 1) : format("nic-${var.resource_identifier}-%03d", count.index + var.resource_name_suffix_start))) : "nic-${var.vm_name_override}"
  location                      = var.location
  resource_group_name           = var.resource_group_name
  enable_accelerated_networking = var.enable_accelerated_networking
  dns_servers                   = var.dns_servers
  tags                          = var.tags

  ip_configuration {
    name                          = var.vm_name_override == "" ? ((var.resource_name_suffix_start == "" ? format("ip-${var.resource_identifier}-%03d", count.index + 1) : format("ip-${var.resource_identifier}-%03d", count.index + var.resource_name_suffix_start))) : "ip-${var.vm_name_override}"
    private_ip_address            = var.private_ip_address_allocation == "Dynamic" ? null : var.private_ip_addresses[count.index]
    private_ip_address_allocation = var.private_ip_address_allocation
    public_ip_address_id          = var.public_ip_enabled == true ? element(azurerm_public_ip.ip.*.id, count.index) : ""
    subnet_id                     = var.subnet_id
  }
}

resource "azurerm_network_interface_security_group_association" "windows_vm" {
  count                     = var.vm_count * (var.enable_nsg == true ? 1 : 0)
  network_interface_id      = element(azurerm_network_interface.windows_vm.*.id, count.index)
  network_security_group_id = var.nsg_id
}

resource "azurerm_network_interface_backend_address_pool_association" "bep_association" {
  count                   = var.vm_count * (var.enable_loadbalancer_bep == true ? 1 : 0)
  network_interface_id    = element(azurerm_network_interface.windows_vm.*.id, count.index)
  ip_configuration_name   = element(azurerm_network_interface.windows_vm.*.ip_configuration.name, count.index)
  backend_address_pool_id = element(var.load_balancer_backend_address_pools_ids, count.index)
}

resource "azurerm_network_interface_application_gateway_backend_address_pool_association" "bep_app_association" {
  count                   = var.vm_count * (var.enable_appgateway_bep == true ? 1 : 0)
  network_interface_id    = element(azurerm_network_interface.windows_vm.*.id, count.index)
  ip_configuration_name   = element(azurerm_network_interface.windows_vm.*.ip_configuration.name, count.index)
  backend_address_pool_id = element(var.application_gateway_backend_address_pools_ids, count.index)
}
