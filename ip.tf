resource "azurerm_public_ip" "ip" {

  count                   = var.vm_count * (var.public_ip_enabled == true ? 1 : 0)
  name                    = var.resource_name_suffix_start == "" ? format("pip-${var.resource_identifier}-%03d", count.index + 1) : format("pip-${var.resource_identifier}-%03d", count.index + var.resource_name_suffix_start)
  location                = var.location
  resource_group_name     = var.resource_group_name
  allocation_method       = var.public_ip_address_allocation
  zones                   = var.az_support == true ? element([["1"], ["2"], ["3"]], count.index) : []
  sku                     = "Standard"
  idle_timeout_in_minutes = var.public_ip_address_timeout
}

