resource "azurerm_virtual_machine_extension" "windows_bootstrap" {
  count                = var.vm_count * (var.bootstrap == true ? 1 : 0)
  name                 = "bootstrap"
  depends_on           = [azurerm_virtual_machine_extension.vm_domain_join, azurerm_windows_virtual_machine.windows_vm]
  publisher            = "Microsoft.Compute"
  type                 = "CustomScriptExtension"
  type_handler_version = "1.7"
  virtual_machine_id   = element(azurerm_windows_virtual_machine.windows_vm.*.id, count.index)

  settings = <<SETTINGS
    {
      "commandToExecute": "powershell -command \"[System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String('${base64encode(local.boot_strap)}')) | Out-File -filepath bootstrap.ps1\" && powershell -ExecutionPolicy Unrestricted -File bootstrap.ps1"
    }
    SETTINGS
}

