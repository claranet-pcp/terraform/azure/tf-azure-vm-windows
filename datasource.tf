data "template_file" "bootstrap" {
  template = file("${path.module}/./include/bootstrap.ps1")

  vars = {
    drive_labels       = join(",", var.drive_labels)
    drive_letters      = join(",", var.drive_letters)
    cdrom_drive_letter = var.cdrom_drive_letter
  }
}
