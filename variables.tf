########## regional ##########

variable location_lookup {
  description = "Azure region to short version lookup"
  type        = map
  default = {
    australiacentral   = "ac"
    australiacentral2  = "ac2"
    australiaeast      = "ae"
    australiasoutheast = "ase"
    brazilsouth        = "bs"
    canadacentral      = "cc"
    canadaeast         = "ce"
    centralindia       = "ci"
    centralus          = "cu"
    eastasia           = "ea"
    eastus             = "eu"
    eastus2            = "eu2"
    francecentral      = "fc"
    francesouth        = "fs"
    japaneast          = "ja"
    japanwest          = "jw"
    koreacentral       = "kc"
    koreasouth         = "ks"
    northcentralus     = "ncu"
    northeurope        = "ne"
    southafricanorth   = "san"
    southafricawest    = "saw"
    southcentralus     = "scu"
    southeastasia      = "sea"
    southindia         = "ai"
    uaecentral         = "uc"
    uaenorth           = "un"
    uksouth            = "us"
    ukwest             = "uw"
    westcentralus      = "wcu"
    westeurope         = "we"
    westindia          = "wi"
    westus             = "wu"
    westus2            = "wu2"
  }
}

variable timezone_lookup {
  description = "Azure region to time zone lookup"
  type        = map
  default = {
    australiacentral   = "AUS Eastern Standard Time"
    australiacentral2  = "AUS Eastern Standard Time"
    australiaeast      = "AUS Eastern Standard Time"
    australiasoutheast = "AUS Eastern Standard Time"
    brazilsouth        = "Central Brazilian Standard Time"
    canadacentral      = "Eastern Standard Time"
    canadaeast         = "Eastern Standard Time"
    centralindia       = "India Standard Time"
    centralus          = "Central Standard Time"
    eastasia           = "China Standard Time"
    eastus             = "Eastern Standard Time"
    eastus2            = "Eastern Standard Time"
    francecentral      = "Romance Standard Time"
    francesouth        = "Romance Standard Time"
    japaneast          = "Tokyo Standard Time"
    japanwest          = "Tokyo Standard Time"
    koreacentral       = "Korea Standard Time"
    koreasouth         = "Korea Standard Time"
    northcentralus     = "Central Standard Time"
    northeurope        = "GMT Standard Time"
    southafricanorth   = "South Africa Standard Time"
    southafricawest    = "South Africa Standard Time"
    southcentralus     = "Central Standard Time"
    southeastasia      = "China Standard Time"
    southindia         = "India Standard Time"
    uaecentral         = "Arab Standard Time"
    uaenorth           = "Arab Standard Time"
    uksouth            = "GMT Standard Time"
    ukwest             = "GMT Standard Time"
    westcentralus      = "Central Standard Time"
    westeurope         = "GMT Standard Time"
    westindia          = "India Standard Time"
    westus             = "Mountain Standard Time"
    westus2            = "Mountain Standard Time"
  }
}


########## virtual machine(s) ##########

## generic settings

variable resource_group_name {
  description = "Specifies the name of the Resource Group in which the VM(s) should exist"
  type        = string
}

variable vm_count {
  description = "The number of VMs you wish to create"
  default     = 1
  type        = number
}

variable location {
  description = "Specifies the Azure Region where the Virtual Machine exists"
  type        = string
  default     = "northeurope"
}

variable vm_size {
  description = "Specifies the Azure Region where the VM(s) exists"
  type        = string
  default     = "Standard_DS1_v2"
}

variable hybrid_benefit_enabled {
  description = "Species enable Azure hybrid benefit"
  type        = bool
  default     = false
}

variable license_type {
  description = "Specifies the BYOL Type for this Virtual Machine"
  type        = string
  default     = "Windows_Server"
}

variable enable_automatic_upgrades {
  description = "Are automatic updates enabled on this VM(s)?"
  type        = string
  default     = false
}

variable vm_name_override {
  default = ""
}

variable enable_loadbalancer_bep {
  description = "Enable nic to be added to loadbalancer backend pool"
  type        = bool
  default     = false
}

variable enable_appgateway_bep {
  description = "Enable nic to be added to Application Gateway backend pool"
  type        = bool
  default     = false
}

variable enable_nsg {
  description = "Enable nsg on nic"
  type        = bool
  default     = false
}

## tags

variable tags {
  description = "A mapping of tags to assign to the VM(s)"
  type        = map
  default     = {}
}

## naming

variable computer_name_suffix_start {
  description = "number from which to start the VM computer name suffix"
  type        = string
  default     = ""
}

variable computer_name {
  description = "computer name"
  type        = string
  default     = ""
}

variable computer_name_override {
  description = "computer name"
  type        = string
  default     = ""
}

variable resource_identifier {
  description = "identifier for Azure VM resource"
  type        = string
  default     = ""
}

variable resource_name_suffix_start {
  description = "number from which to start the resource id suffix"
  type        = string
  default     = ""
}

########## disks ##########

## generic

variable vm_disk_type {
  description = "Specifies the type of managed disk to create for all drives"
  type        = string
}

variable cdrom_drive_letter {
  description = "Specifies a drive letter on which to mount the CD-ROM"
  type        = string
  default     = "v"
}

variable system_identity_enabled {
  description = "(Required) The Managed Service Identity Type of this Virtual Machine. Possible values are SystemAssigned (where Azure will generate a Service Principal for you)"
  type        = bool
  default     = false
}

## os_disk

variable os_caching {
  description = "Specifies the caching requirements for the OS Disk"
  type        = string
  default     = "ReadWrite"
}

variable os_create_option {
  description = "Specifies how the OS Disk should be created"
  type        = string
  default     = "FromImage"
}

variable delete_os_disk_on_termination {
  description = "Should the OS Disk (either the Managed Disk / VHD Blob) be deleted when the Virtual Machine is destroyed?"
  type        = bool
  default     = true
}

## storage_data_disk

variable drive_letters {
  description = "Specifies a list of drive letters to mount on the VM(s)"
  type        = list
  default     = []
}

variable drive_sizes {
  description = "Specifies a list of drive sizes to mount on the VM(s)"
  type        = list
  default     = []
}

variable drive_labels {
  description = "Specifies a list of drive labels to mount on the VM(s)"
  type        = list
  default     = []
}

variable delete_data_disks_on_termination {
  description = "Should the Data Disks (either the Managed Disks / VHD Blobs) be deleted when the Virtual Machine is destroyed?"
  type        = bool
  default     = true
}

########## backups ##########

## settings

variable backup_enabled {
  description = "Enable VM backups"
  type        = bool
  default     = false
}

variable recovery_vault_rg_name {
  description = "The name of the resource group in which to create the Recovery Services Protected VM(s)"
  type        = string
  default     = ""
}

variable recovery_vault_name {
  description = "Specifies the name of the Recovery Services Vault to use"
  type        = string
  default     = ""
}

variable backup_policy_id {
  description = "Specifies the id of the backup policy to use"
  type        = string
  default     = ""
}

########## automation ##########

## settings

variable bootstrap {
  description = "Enable boostrapping of VM(s)"
  type        = bool
  default     = false
}

variable boot_script {
  description = "Specifies rendered script to be run on 1st boot"
  type        = string
  default     = ""
}

########## placement ##########

## availability zones

variable az_support {
  description = "Enable az support"
  type        = bool
  default     = true
}

## availability sets

variable availability_set {
  description = "Enables Availability Set"
  type        = bool
  default     = false
}

variable availability_set_id {
  description = "Availability Set id. If not passed then new Availability set is created"
  type        = string
  default     = null
}

variable platform_fault_domain_count {
  description = "Specifies the number of fault domains that are used"
  type        = number
  default     = 3
}

variable platform_update_domain_count {
  description = "Specifies the number of update domains that are use"
  type        = number
  default     = 5
}

variable diags_storage_account_uri {
  description = "The Storage Account's Blob Endpoint which should hold the VM(s) diagnostic files"
  type        = string
  default     = ""
}

########## operating system ##########

## image

variable image_publisher {
  description = "Specifies the publisher of the image used to create the VM(s)"
  type        = string
  default     = "MicrosoftWindowsServer"
}

variable image_offer {
  description = "Specifies the offer of the image used to create the VM(s)"
  type        = string
  default     = "WindowsServer"
}

variable image_sku {
  description = "Specifies the SKU of the image used to create the VM(s)"
  type        = string
  default     = "2016-Datacenter"
}

variable image_version {
  description = "Specifies the version of the image used to create the VM(s)"
  type        = string
  default     = "latest"
}

variable custom_image_id {
  description = "Specifies the ID of the Custom Image which the VM(s) should be created from"
  type        = string
  default     = null
}

## admin

variable admin_username {
  description = "Specifies the name of the local administrator account"
  type        = string
  default     = "azure.admin"
}

variable admin_password {
  description = "The password associated with the local administrator account"
  type        = string
}


########## network interface(s) ##########

## connected resources

variable subnet_id {
  description = "Reference to a subnet in which the NIC(s) have been created"
  type        = string
}

variable nsg_id {
  description = "The ID of the Network Security Group to associate with the network interface(s)"
  type        = string
  default     = null
}

variable application_gateway_backend_address_pools_ids {
  description = "List of Application Gateway Backend Address Pool IDs references to which this NIC(s) belong"
  type        = list
  default     = null
}

variable load_balancer_backend_address_pools_ids {
  description = "List of Load Balancer Backend Address Pool IDs references to which this NIC(s) belong"
  type        = list
  default     = []
}

variable dns_servers {
  description = "(Optional) A list of IP Addresses defining the DNS Servers which should be used for this Network Interface"
  type        = list
  default     = null
}

## configuration

variable public_ip_enabled {
  description = "Enable public ip address"
  type        = bool
  default     = false
}

variable public_ip_address_allocation {
  description = "Defines how a public IP address(es) is assigned"
  type        = string
  default     = "Static"
}

variable public_ip_address_timeout {
  description = "Defines the timeout for the public IP address(es) (in minutes)"
  type        = number
  default     = 4
}

variable private_ip_address_allocation {
  description = "Defines how a private IP address(es) is assigned"
  type        = string
  default     = "Dynamic"
}

variable private_ip_addresses {
  description = "Define a custom private IP address"
  type        = list
  default     = null
}

variable enable_accelerated_networking {
  description = "Enables Azure Accelerated Networking using SR-IOV"
  type        = bool
  default     = false
}

########## extensions/agents ##########

## VM agent

variable provision_vm_agent {
  description = "Should the Azure Virtual Machine Guest Agent be installed on this VM(s)?"
  type        = string
  default     = true
}

## domain join

variable domain_join {
  description = "Enable domain join"
  type        = bool
  default     = false
}

variable ad_domain_name {
  description = "Specifies active directory domain name"
  type        = string
  default     = ""
}

variable ad_domain_join_user {
  description = "Specifies user to join computers to domain"
  type        = string
  default     = ""
}

variable domain_join_pw {
  description = "Specifies domain join password"
  type        = string
  default     = ""
}

variable ad_ou_path {
  description = "Specifies ou to deploy new computer account(s)"
  type        = string
  default     = ""
}


## oms extension

variable oms_extension {
  description = "Enable OMS extension"
  type        = bool
  default     = true
}

variable log_analytics_workspace_id {
  description = "Log Analytics Workspace id for OMS extension"
  type        = string
  default     = ""
}

variable log_analytics_workspace_key {
  description = "Log Analytics Workspace key for OMS extension"
  type        = string
  default     = ""
}

## iaas diags 

variable iaas_diags_extension {
  description = "Enable IaaS Diags extension"
  type        = bool
  default     = false
}

variable storage_account_name {
  description = "IaaS Diags extension Storage Account name"
  type        = string
  default     = ""
}

variable storage_account_key {
  description = "IaaS Diags extension Storage Account key"
  type        = string
  default     = ""
}

## sql iaas

variable sql_iaas_extension_enable {
  description = "Enable the SQL IaaS agent for automated backups and patching of SQL Server"
  type        = bool
  default     = false
}

variable sql_enable_auto_patch {
  description = "Enable or disable automatic SQL patching"
  type        = bool
  default     = true
}

variable sql_patch_day {
  description = "Day of week to automatically patch SQL"
  type        = string
  default     = "Sunday"
}

variable sql_patch_start_hour {
  description = "Maintenance window start hour"
  type        = number
  default     = 0
}

variable sql_patch_duration {
  description = "Maintenance window duration in minutes"
  type        = number
  default     = 60
}

variable sql_enable_auto_backup {
  description = "Enable SQL auto backup"
  type        = bool
  default     = true
}

variable sql_backup_retention {
  description = "Retention in days of automated SQL backups"
  type        = number
  default     = 30
}

variable sql_backup_encryption {
  description = "Enable or disable encryption of sql backups"
  type        = bool
  default     = true
}

variable sql_backup_systemdbs {
  description = "Enable or disable backups of system dbs"
  type        = bool
  default     = true
}

variable sql_backup_storage_account {
  description = "Storage account url"
  type        = string
  default     = ""
}

variable sql_backup_encryption_password {
  description = "Password used to encrypt SQL backups"
  type        = string
  default     = ""
}

variable sql_backup_storage_access_key {
  description = "Storage account access key"
  type        = string
  default     = ""
}

## dependency agent

variable dependency_agent_enabled {
  description = "Enable or disable the dependency agent"
  type        = bool
  default     = false
}
