output "nic" {
  value = azurerm_network_interface.windows_vm
}

output "vm" {
  value = azurerm_windows_virtual_machine.windows_vm
}
#remove below
output "datadisks" {
  value = azurerm_managed_disk.datadisk
}

output disk_attachments {
  value = local.disk_list_of_maps
}
